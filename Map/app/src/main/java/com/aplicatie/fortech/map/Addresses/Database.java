package com.aplicatie.fortech.map.Addresses;

import com.aplicatie.fortech.map.StructArray.Struct;

import java.util.ArrayList;

/**
 * Created by Andrei on 8/19/2015.
 */
public class Database {
    ArrayList<Address> _locations = new ArrayList<>();
    public Database(){
        _locations.add(new Address("Fortech", new Struct(46.752160, 23.575946)));
        _locations.add(new Address("Strada Mihail Kogalniceanu Nr 1", new Struct(46.767447, 23.591655)));
        _locations.add(new Address("Centru", new Struct(46.770321, 23.589314)));
        _locations.add(new Address("Liceul Teoretic Avram Iancu", new Struct(46.7773778,23.6014778)));
        _locations.add(new Address("Parcul Central", new Struct(46.7687438,23.5774237)));
        _locations.add(new Address("Opera Nationala Romana Cluj-Napoca", new Struct(46.769985, 23.597511)));
        _locations.add(new Address("Polus Center", new Struct(46.750521, 23.533017)));
        _locations.add(new Address("Iulius Mall", new Struct(46.771437, 23.625131)));
    }

    public ArrayList<Address> getLocations(){
        return this._locations;
    }
}