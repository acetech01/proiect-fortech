package com.aplicatie.fortech.map.StructArray;

/**
 * Created by Dora on 7/23/2015.
 */
public class Struct {
    private double _lat;
    private double _lon;

    public Struct(double lat, double lon){
        _lat = lat;
        _lon = lon;
    }

    public void addCoordinates(double lat, double lon){
        _lat = lat;
        _lon = lon;
    }
    public double getLat(){
        return _lat;
    }
    public double getLon(){
        return _lon;
    }
}