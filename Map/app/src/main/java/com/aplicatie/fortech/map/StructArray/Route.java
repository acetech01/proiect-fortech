package com.aplicatie.fortech.map.StructArray;

/**
 * Created by Andrei Chis on 7/23/2015.
 */
import java.util.ArrayList;

/**
 *
 * @author Dora
 */
public class Route {
    String _name;
    int _color;
    ArrayList<Struct>  _stations = new ArrayList<> ();

    public Route(String name, int color){
        this._name = name;
        this._color = color;
    }

    public void addStation(double lat, double longit){
        Struct newStation = new Struct(lat, longit);
        this._stations.add(newStation);
    }

    public ArrayList<Struct> getStations(){
        return this._stations;

    }

    public int getColor(){
        return this._color;
    }

    public String getName(){
        return this._name;
    }
}

