package com.aplicatie.fortech.map.Algorithm;

import com.aplicatie.fortech.map.StructArray.Route;
import com.aplicatie.fortech.map.StructArray.Struct;

/**
 * Created by Andrei on 20.07.2015.
 *            BO$$
 */
public class Algo {

    public double distanceBetweenCoordinates(Struct point1, Struct point2) {
        double R = 6371; //Radius of the earth in km 6371
        double dLat = Math.toRadians(point2.getLat() - point1.getLat());
        double dLon = Math.toRadians(point2.getLon() - point1.getLon());
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(Math.toRadians(point1.getLat())) * Math.cos(Math.toRadians(point2.getLat())) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;
        double meterConversion = 1000; //transforming from kilometers to meters.
        return d * meterConversion; //Distance in meters.

    }

    private int compareRoutes(double jos1, double jos2, double bus1, double bus2) {
        double  vcjos = 1500, vcbus = 10000, jos1p , jos2p , bus1p, bus2p;

        jos1 = (jos1*0.5)/vcjos;
        jos2 = (jos2*0.5)/vcjos;
        bus1 = (bus1*0.5)/vcbus;
        bus2 = (bus2*0.5)/vcbus;

        jos1p = jos1*jos1;
        jos2p = jos2*jos2;
        bus1p = bus1*bus1;
        bus2p = bus2*bus2;

        jos1 = (1-jos1)*(1-jos1);
        jos2 = (1-jos2)*(1-jos2);
        bus1 = (1-bus1)*(1-bus1);
        bus2 = (1-bus2)*(1-bus2);

        jos1p = jos1p+bus1p;
        jos2p = jos2p+bus2p;
        jos1 = jos1+bus1;
        jos2 = jos2+bus2;

        jos1p = Math.sqrt(jos1p);
        jos2p = Math.sqrt(jos2p);
        jos1 = Math.sqrt(jos1);
        jos2 = Math.sqrt(jos2);

        jos1 = jos1/(jos1+jos1p);
        jos2 = jos2/(jos2+jos2p);

        if((1-jos1)<(1-jos2)) return 1;
        else return 2;

    }

    public int stations_to_travel;

    public Route findBestRoute(Route r1, Route r2, Struct start, Struct end) {
        double closestToA1[], closestToA2[], closestToB1[], closestToB2[], bus_distR1 = 0, bus_distR2 = 0,
                foot_distA1, foot_distB1, foot_distA2, foot_distB2;

        closestToA1 = findClosestStationToPointInRoute(start, r1);
        closestToB1 = findClosestStationToPointInRoute(end, r1);
        closestToA2 = findClosestStationToPointInRoute(start, r2);
        closestToB2 = findClosestStationToPointInRoute(end, r2);
        /**
         * because the closest station to B might be on a different array that the array containing the closest station to A,
         * we find the closest stations to A and B on both arrays, then we compare the travelled distances via the compareRoute mehod
         */
        for (int i = (int) Math.min(closestToA1[0], closestToB1[0]); i < Math.max(closestToA1[0] - 1, closestToB1[0]); ++i)
            bus_distR1 += distanceBetweenCoordinates(r1.getStations().get(i), r2.getStations().get(i + 1));

        for (int i = (int) Math.min(closestToA2[0], closestToB2[0]); i < Math.max(closestToA2[0] - 1, closestToB2[0]); ++i)
            bus_distR2 += distanceBetweenCoordinates(r2.getStations().get(i), r2.getStations().get(i + 1));
        foot_distA1 = closestToA1[1];
        foot_distA2 = closestToA2[1];
        foot_distB1 = closestToB1[1];
        foot_distB2 = closestToB2[1];

        if (compareRoutes(foot_distA1 + foot_distB1 , foot_distA2 + foot_distB2 , bus_distR1 , bus_distR2) == 1) {
            stations_to_travel = (int)(Math.max(closestToA1[0], closestToB1[0]) - Math.min(closestToA1[0], closestToB1[0]));
            return r1;
        }
        else {
            stations_to_travel = (int)(Math.max(closestToA2[0], closestToB2[0]) - Math.min(closestToA2[0], closestToB2[0]));
            return r2;
        }
    }

    private double[] findClosestStationToPointInRoute(Struct point, Route r){
        double positionDistance[] = new double[2];
        positionDistance[0] = 0;
        positionDistance[1] = 100000000;

        for (int i = 0; i < r.getStations().size(); ++i){
            double currentDistance = distanceBetweenCoordinates(point, r.getStations().get(i));
            if (currentDistance < positionDistance[1]) {
                positionDistance[1] = currentDistance;
                positionDistance[0] = i;
            }
        }
        return positionDistance;
    }
}