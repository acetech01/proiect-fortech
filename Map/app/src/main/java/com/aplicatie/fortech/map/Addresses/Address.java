package com.aplicatie.fortech.map.Addresses;

import com.aplicatie.fortech.map.StructArray.Struct;

/**
 * Created by Andrei BO$$ on 8/18/2015.
 */
public class Address{
    String _name;
    Struct _position;

    public  Address(String name, Struct position){
        _name = name;
        _position = position;
    }

    public String getName(){
        return _name;
    }

    public Struct getLocation(){
        return _position;
    }
}