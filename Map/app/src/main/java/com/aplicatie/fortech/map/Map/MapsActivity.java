package com.aplicatie.fortech.map.Map;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aplicatie.fortech.map.Addresses.Address;
import com.aplicatie.fortech.map.Addresses.Database;
import com.aplicatie.fortech.map.Algorithm.Algo;
import com.aplicatie.fortech.map.R;
import com.aplicatie.fortech.map.StructArray.*;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity {
    //<editor-fold desc="Fields and Variables">
    private GoogleMap mMap;
    RouteCoordinates routeCoordinates = new RouteCoordinates();
    int walking_distance_limit = 500;
    public static final Struct latLngCluj = new Struct(46.7699056, 23.5913758);
    private static Struct start_point = new Struct(46.7699056, 23.5913758);
    public static Struct end_point;
    //</editor-fold>

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMap();

        drawStationsAndPolyline(routeCoordinates.route_9);
        drawStationsAndPolyline(routeCoordinates.route_35);
        /*start_point = new Struct(currentLocation().latitude, currentLocation().longitude);
        mMap.addMarker(new MarkerOptions()
                .position(currentLocation())
                .draggable(false)
                .title("You are here! :)")
                .snippet(currentLocation().toString())
        );*/
        addMarker(start_point, "Departure point", 100);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latLngCluj.getLat(), latLngCluj.getLon())));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMap();
    }

    private void setUpMap() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map))
                    .getMap();
        }
    }

    private LatLng currentLocation() {
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location myLocation = locationManager.getLastKnownLocation(provider);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        return new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
    }

    private void addBusStationMarkerAtPosition(Struct position, String route_name) {
        mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(position.getLat(), position.getLon()))
                        .draggable(false)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_red_bus_station))
                        .title(route_name)
                        .anchor(0.5f, 0.5f)
        );
    }

    private void drawStationsAndPolyline(Route r) {
        ArrayList<LatLng> line = new ArrayList<>();
        for (int i = 0; i < r.getStations().size(); ++i) {
            addBusStationMarkerAtPosition(r.getStations().get(i), r.getName());
            line.add(new LatLng(r.getStations().get(i).getLat(), r.getStations().get(i).getLon()));
        }
        Polyline polyline = mMap.addPolyline(new PolylineOptions()
                        .addAll(line)
                        .width(5)
                        .color(r.getColor())
        );
    }

    private void addMarker(Struct position, String title, int color) {
        mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(position.getLat(), position.getLon()))
                        .title(title)
                        .icon(BitmapDescriptorFactory.defaultMarker(color))
        );
    }

    public void findBusOnClick(View view) {
        mMap.clear();
        addMarker(start_point, "Departure point", 100);
        end_point = new Struct(0, 0);
        Route foundRoute = null;
        drawStationsAndPolyline(routeCoordinates.route_9);
        drawStationsAndPolyline(routeCoordinates.route_35);
        boolean found = false;
        Database database = new Database();
        Algo algorithm = new Algo();
        EditText inputText = (EditText) findViewById(R.id.editText);
        String searchBarInputText = inputText.getText().toString();

        if (searchBarInputText != null) {
            int database_size = database.getLocations().size();

            for (int i = 0; i < database_size; ++i) {
                if (searchBarInputText.contentEquals(database.getLocations().get(i).getName())) {
                    end_point = database.getLocations().get(i).getLocation();
                    found = true;
                    break;
                }
            }

            if (found) {
                addMarker(end_point, "Destination point", 200);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(end_point.getLat(), end_point.getLon())));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(14));
                foundRoute = algorithm.findBestRoute(routeCoordinates.route_9, routeCoordinates.route_35, start_point, end_point);

                if (algorithm.distanceBetweenCoordinates(start_point, end_point) <= walking_distance_limit || algorithm.stations_to_travel == 0) {
                    Toast.makeText(getApplicationContext(), "There is no need for a bus; you can walk, it's healthy!", Toast.LENGTH_LONG).show();
                } else Toast.makeText(getApplicationContext(), "Take " + foundRoute.getName() + " from the nearest station and travel " + algorithm.stations_to_travel + " stations.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Nothing found", Toast.LENGTH_SHORT).show();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(start_point.getLat(), start_point.getLon())));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(14));
            }
        }
    }
}