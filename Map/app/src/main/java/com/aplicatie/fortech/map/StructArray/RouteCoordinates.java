package com.aplicatie.fortech.map.StructArray;
import android.graphics.Color;

import com.aplicatie.fortech.map.Map.MapsActivity;
/**
 * Created by Andrei on 28.07.2015.
 *            BO$$
 */

public class RouteCoordinates {

    public Route route_9 = new Route("Line 9", Color.GREEN);
    public Route route_35 = new Route("Line 35", Color.BLUE);

    public RouteCoordinates(){
        route_9.addStation(46.752180, 23.543540);
        route_9.addStation(46.757752, 23.546501);
        route_9.addStation(46.758619, 23.550965);
        route_9.addStation(46.760810, 23.564204);
        route_9.addStation(46.763397, 23.573130);
        route_9.addStation(46.766395, 23.580147);
        route_9.addStation(46.769599, 23.586906);
        route_9.addStation(46.777784, 23.587328);
        route_9.addStation(46.784206, 23.588259);

        route_35.addStation(46.752751, 23.577970);
        route_35.addStation(46.754407, 23.584024);
        route_35.addStation(46.755728, 23.593227);
        route_35.addStation(46.760847, 23.597750);
        route_35.addStation(46.760847, 23.597750);
        route_35.addStation(46.771315, 23.597365);
        route_35.addStation(46.777784, 23.587328);
        route_35.addStation(46.784206, 23.588259);
        route_35.addStation(46.780842, 23.591611);
    }
}
